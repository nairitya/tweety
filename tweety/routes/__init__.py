from tweety import app
from flask import Flask, jsonify, request, render_template
from flask import make_response, url_for, redirect
import twitter
import config

configuration = config.config()
api=twitter.Api(consumer_key=configuration['consumer_key'], consumer_secret=configuration['consumer_secret'], access_token_key=configuration['access_token_key'], access_token_secret=configuration['access_token_secret'])

@app.route('/')

def home():
	return render_template("home.html")

@app.route('/follower', methods=['POST'])
def get_follower():
	users = api.GetFollowers(screen_name=request.form['user'])
	return render_template('follower.html', follwer=users, count=len(users))

@app.route('/msg')
def get_friends():
	user = request.args["u"]
	return render_template('msg.html', user=user)

@app.route('/send', methods=['POST'])
def send_message():
	user = request.form["user"]
	msg = request.form["msg"]

	status = api.PostUpdate("@"+user+" "+msg)
	return status.text
